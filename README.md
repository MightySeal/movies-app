# Demo movies app
<img src="media/demo.gif" alt="demo video" width="300" />

# About

The app shows the list of movies from [api](https://movies-sample.herokuapp.com/api/movies), provides basic name and genre search and holds data in a basic cache.
(Please not, that Lucy and Power Rangers have the same id of `8666` which affect cache, power rangers simply overwrite Lucy)

# Implementation details

The is built on the top of compose, dagger, kotlin coroutines and follows ideas clean architecture in terms of layering and TEA/MVI in terms of data handling.
However, there are a few extensions such as the idea of using flow as the core element. If you look at `MoviesListViewModel`, you'll see that the search input is represented as flow which is transformed to something different.
This approach gives several benefits:
* Thread safety and proper data handling. Using flows it's pretty simple to establish proper _happens before_ relationship between different emits due to coroutines nature, so it's simple to show data in proper order.
* Flows are reactive by nature, so when we face errors (i.e. network error, server error, etc.) it's pretty simple to setup retry policy, for instance, download the data when the connection is established again. It is not implemented in this demo, but can be done relatively simple.

# Testing
There are a few tests, `MoviesRepositoryImplTest` and `ListScreenTest`. 