plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-parcelize")
    id("dagger.hilt.android.plugin")
}


android {
    compileSdk = 31
    defaultConfig {
        applicationId = "io.kzolotov.moviesapp"
        minSdk = 24
        targetSdk = 31
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        release {
            isMinifyEnabled = false
        }
    }
    packagingOptions {
        pickFirst("META-INF/kotlinx-coroutines-core.kotlin_module")
        pickFirst("META-INF/AL2.0")
        pickFirst("META-INF/LGPL2.1")
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = Versions.COMPOSE_VERSION
    }
    kotlinOptions {
        jvmTarget = "11"
    }
}

dependencies {
    implementation(Libs.COROUTINES_ANDROID)
    implementation(Libs.OKIO)

    implementation(project(":domain"))
    implementation(project(":data"))
    implementation(project(":design"))

    implementation("androidx.compose.ui:ui:${Versions.COMPOSE_VERSION}")
    implementation("androidx.activity:activity-compose:${Versions.ACTIVITY_COMPOSE_VERSION}")
    implementation("androidx.compose.ui:ui-tooling:${Versions.COMPOSE_VERSION}")
    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:2.4.0-rc01")
    implementation("androidx.compose.material:material-icons-extended:${Versions.COMPOSE_VERSION}")

    implementation("androidx.compose.animation:animation-core:${Versions.COMPOSE_VERSION}")
    implementation("androidx.compose.animation:animation:${Versions.COMPOSE_VERSION}")

    implementation("androidx.compose.material:material-icons-core:${Versions.COMPOSE_VERSION}")
    implementation("androidx.compose.material:material-icons-extended:${Versions.COMPOSE_VERSION}")
    implementation("androidx.compose.material:material-ripple:${Versions.COMPOSE_VERSION}")
    implementation("androidx.compose.material:material:${Versions.COMPOSE_VERSION}")
    implementation("io.coil-kt:coil-compose:1.3.2")

    implementation("androidx.constraintlayout:constraintlayout-compose:${Versions.CONSTRAINT_COMPOSE_VERSION}")

    implementation("androidx.appcompat:appcompat:1.4.0-alpha03")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:2.3.1")
    implementation("com.google.android.material:material:1.4.0")

    implementation(Libs.TIMBER)

    // DI
    implementation(Libs.HILT)
    kapt(Libs.HILT_COMPILER)
    kapt("androidx.hilt:hilt-compiler:1.0.0")

    implementation("androidx.legacy:legacy-support-v4:1.0.0")

    androidTestImplementation("org.jetbrains.kotlin:kotlin-test-junit:${Versions.KOTLIN_VERSION}")
    androidTestImplementation("androidx.test:runner:1.4.0")
    androidTestImplementation("androidx.compose.ui:ui-test-junit4:${Versions.COMPOSE_VERSION}")
    androidTestImplementation("androidx.test:core:1.4.0")
    debugImplementation("androidx.compose.ui:ui-test-manifest:${Versions.COMPOSE_VERSION}")

}

hilt {
    enableExperimentalClasspathAggregation = true
}
