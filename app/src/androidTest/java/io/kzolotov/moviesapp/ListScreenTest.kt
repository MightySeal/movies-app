package io.kzolotov.moviesapp

import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performTextInput
import io.kzolotov.moviesapp.design.Themed
import io.kzolotov.moviesapp.domain.model.Movie
import io.kzolotov.moviesapp.list.ContentScreen
import io.kzolotov.moviesapp.list.LoadingScreen
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class ListScreenTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()


    @Test
    fun content_displays_title_genre_year() {
        composeTestRule.setContent {
            Themed {
                ContentScreen(moviesStub, "") {}
            }
        }

        composeTestRule.onNodeWithText("Dunkirk").assertExists("Should display title")
        composeTestRule.onNodeWithText("History, 2017")
            .assertExists("Should display genre and year")
    }


    @Test
    fun input_calls_updates() {
        var input = ""
        composeTestRule.setContent {
            Themed {
                ContentScreen(moviesStub, "") {
                    input = it
                }
            }
        }

        composeTestRule.onNodeWithText("Search", substring = true, ignoreCase = true)
            .performTextInput("Search input")
        assertEquals("Search input", input)
    }

    @Test
    fun loading_screen_hierarchy_test() {
        composeTestRule.setContent {
            Themed {
                LoadingScreen()
            }
        }

        composeTestRule.onNodeWithText("Loading").assertExists("Loading should be displayed")
        composeTestRule.onNodeWithText("Error").assertDoesNotExist()
    }


    private val moviesStub = listOf(
        Movie(
            id = 912312,
            title = "Dunkirk",
            year = 2017,
            genre = "History",
            poster = "https://goo.gl/1zUyyq",
        )
    )

}