package io.kzolotov.moviesapp.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import io.kzolotov.moviesapp.domain.interactor.MoviesInteractor
import io.kzolotov.moviesapp.domain.model.Movie
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@OptIn(ExperimentalCoroutinesApi::class)
@HiltViewModel
class MoviesListViewModel @Inject constructor(
    private val moviesInteractor: MoviesInteractor
) : ViewModel() {

    private val searchInput = MutableSharedFlow<String>()
    private val state = logic()

    fun state() = state

    fun search(query: String) {
        viewModelScope.launch {
            searchInput.emit(query)
        }
    }

    private fun logic(): Flow<MoviesListState> {
        return searchInput.onStart { emit("") }
            .flatMapLatest { query ->
                if (query.isBlank()) {
                    moviesInteractor.getAll()
                } else {
                    moviesInteractor.query(query)
                }.map<List<Movie>, MoviesListState> {
                    MoviesListState.Data(
                        MoviesState(
                            query = query,
                            movies = it
                        )
                    )
                }.catch { emit(MoviesListState.Error(null)) }
            }
            .onStart { emit(MoviesListState.Loading()) }
            .runningReduce { prev, current ->
                when (current) {
                    is MoviesListState.Data -> current
                    is MoviesListState.Loading -> current.copy(prev.movies)
                    is MoviesListState.Error -> current.copy(prev.movies)
                }
            }
            .flowOn(Dispatchers.Default)
            .shareIn(scope = viewModelScope, started = SharingStarted.Eagerly, replay = 1)
    }

    data class MoviesState(
        val query: String,
        val movies: List<Movie>,
    )

    sealed class MoviesListState {
        abstract val movies: MoviesState?

        data class Loading(override val movies: MoviesState? = null) : MoviesListState()
        data class Error(override val movies: MoviesState? = null) : MoviesListState()
        data class Data(override val movies: MoviesState) : MoviesListState()
    }
}