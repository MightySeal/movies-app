package io.kzolotov.moviesapp.list

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Scaffold
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.SnackbarHostState
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import coil.transform.RoundedCornersTransformation
import io.kzolotov.moviesapp.R
import io.kzolotov.moviesapp.design.AppTheme
import io.kzolotov.moviesapp.design.CrimsonRed
import io.kzolotov.moviesapp.design.White
import io.kzolotov.moviesapp.design.component.AppText
import io.kzolotov.moviesapp.design.component.TextInput
import io.kzolotov.moviesapp.domain.model.Movie
import io.kzolotov.moviesapp.list.MoviesListViewModel.MoviesListState

@Composable
fun MoviesListScreen(
    viewModel: MoviesListViewModel
) {
    val moviesState =
        viewModel.state()
            .collectAsState(MoviesListState.Loading())

    var searchQuery by rememberSaveable { mutableStateOf("") }

    when (val state = moviesState.value) {
        is MoviesListState.Loading -> LoadingScreen()
        is MoviesListState.Error -> ErrorScreen(state.movies?.movies ?: emptyList(), searchQuery) {
            searchQuery = it
            viewModel.search(it)
        }
        is MoviesListState.Data -> ContentScreen(state.movies.movies, searchQuery) {
            searchQuery = it
            viewModel.search(it)
        }
    }
}

@OptIn(ExperimentalFoundationApi::class, ExperimentalCoilApi::class)
@Composable
fun ContentScreen(movies: List<Movie>, searchQuery: String, onSearch: (String) -> Unit) {
    Column(
        modifier = Modifier.padding(16.dp)
    ) {
        TextInput(
            label = "Search by title or genre",
            searchQuery = searchQuery,
            onInput = onSearch,
            modifier = Modifier.fillMaxWidth()
        )

        if (movies.isEmpty()) {
            Box(modifier = Modifier.fillMaxSize()) {
                AppText(text = "Nothing to show", modifier = Modifier.align(Alignment.Center))
            }
        } else {
            LazyVerticalGrid(
                cells = GridCells.Fixed(3),
                modifier = Modifier
                    .fillMaxSize()
                    .padding(top = 7.5.dp),
            ) {
                items(movies) { movie ->
                    Column(
                        modifier = Modifier.padding(8.dp)
                    ) {
                        Image(
                            painter = rememberImagePainter(
                                data = movie.poster,
                                builder = {
                                    placeholder(R.drawable.outline_broken_image_24)
                                    error(R.drawable.outline_broken_image_24)
                                    transformations(RoundedCornersTransformation(16.dp.value))
                                    crossfade(true)
                                }
                            ),
                            contentDescription = null,
                            modifier = Modifier
                                .height(160.dp)
                                .fillMaxWidth()
                        )

                        AppText(
                            text = highlightedText(movie.title, searchQuery),
                            style = AppTheme.typography.h6.copy(fontSize = 14.sp)
                        )
                        AppText(
                            text = highlightedText("${movie.genre}, ${movie.year}", searchQuery),
                            style = AppTheme.typography.subtitle2.copy(fontSize = 12.sp)
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun LoadingScreen() {
    Box(modifier = Modifier.fillMaxSize()) {
        AppText(text = "Loading", modifier = Modifier.align(Alignment.Center))
    }
}

@Composable
fun ErrorScreen(movies: List<Movie>, searchQuery: String, onSearch: (String) -> Unit) {

    val snackbarHostState = remember { SnackbarHostState() }

    if (movies.isNotEmpty()) {
        LaunchedEffect(true) {
            snackbarHostState.showSnackbar(
                message = "Showing cached data",
                duration = SnackbarDuration.Indefinite
            )
        }
    }

    Scaffold(
        scaffoldState = rememberScaffoldState(snackbarHostState = snackbarHostState)
    ) { innerPadding ->
        Box(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize()
                .background(AppTheme.colors.background)
        ) {
            ContentScreen(movies, searchQuery, onSearch)
        }
    }
}

private fun highlightedText(text: String, highlight: String): AnnotatedString {
    val highlightStart = text.indexOf(highlight, ignoreCase = true)
    return if (highlightStart != -1) {
        val before = text.substring(0, highlightStart)
        val highLighted = text.substring(highlightStart, highlightStart + highlight.length)
        val after = text.substring(highlightStart + highlight.length, text.length)

        buildAnnotatedString {
            withStyle(style = SpanStyle(color = White)) {
                append(before)
            }
            withStyle(SpanStyle(color = CrimsonRed)) {
                append(highLighted)
            }
            withStyle(style = SpanStyle(color = White)) {
                append(after)
            }
        }
    } else {
        buildAnnotatedString {
            append(text)
        }
    }
}