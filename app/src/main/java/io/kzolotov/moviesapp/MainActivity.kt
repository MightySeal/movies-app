package io.kzolotov.moviesapp

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.Modifier
import dagger.hilt.android.AndroidEntryPoint
import io.kzolotov.moviesapp.design.AppTheme
import io.kzolotov.moviesapp.design.Themed
import io.kzolotov.moviesapp.list.MoviesListScreen
import io.kzolotov.moviesapp.list.MoviesListViewModel

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private val exampleViewModel: MoviesListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Themed {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(color = AppTheme.colors.background),
                ) {
                    MoviesListScreen(exampleViewModel)
                }
            }
        }
    }
}