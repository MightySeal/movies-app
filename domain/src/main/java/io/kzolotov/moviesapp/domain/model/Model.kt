package io.kzolotov.moviesapp.domain.model

data class Movie(
    val id: Long,
    val title: String,
    val year: Int,
    val genre: String,
    val poster: String,
    val timestamp: Long? = null
)