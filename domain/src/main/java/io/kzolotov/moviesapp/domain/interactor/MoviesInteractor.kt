package io.kzolotov.moviesapp.domain.interactor

import io.kzolotov.moviesapp.domain.model.Movie
import io.kzolotov.moviesapp.domain.repository.MoviesRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoviesInteractor @Inject constructor(
    private val moviesRepository: MoviesRepository
) {
    fun getAll(): Flow<List<Movie>> {
        return moviesRepository.getAll()
    }

    fun query(query: String): Flow<List<Movie>> {
        return moviesRepository.query(query)
    }
}