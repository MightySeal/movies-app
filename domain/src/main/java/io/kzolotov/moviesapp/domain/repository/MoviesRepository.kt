package io.kzolotov.moviesapp.domain.repository

import io.kzolotov.moviesapp.domain.model.Movie
import kotlinx.coroutines.flow.Flow

interface MoviesRepository {
    fun getAll(): Flow<List<Movie>>
    fun query(query: String): Flow<List<Movie>>
}