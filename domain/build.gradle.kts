plugins {
    kotlin("jvm")
    id("kotlin-kapt")
}

dependencies {
    implementation(Libs.COROUTINES)
    implementation("javax.inject:javax.inject:1")
}
