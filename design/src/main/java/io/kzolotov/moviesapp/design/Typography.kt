package io.kzolotov.moviesapp.design

import androidx.compose.material.Typography
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight

val ArimoFontFamily = FontFamily(
    Font(resId = R.font.arimo_regular, weight = FontWeight.Normal, style = FontStyle.Normal),
    Font(resId = R.font.arimo_semi_bold, weight = FontWeight.SemiBold, style = FontStyle.Normal),
    Font(resId = R.font.arimo_bold, weight = FontWeight.Bold, style = FontStyle.Normal)
)

val AppFontFamily = ArimoFontFamily

val AppTypography = Typography().withFontFamily(AppFontFamily)

private fun Typography.withFontFamily(fontFamily: FontFamily): Typography = this.copy(
    h1 = this.h1.copy(fontFamily = fontFamily, color = White),
    h2 = this.h2.copy(fontFamily = fontFamily, color = White),
    h3 = this.h3.copy(fontFamily = fontFamily, color = White),
    h4 = this.h4.copy(fontFamily = fontFamily, color = White),
    h5 = this.h5.copy(fontFamily = fontFamily, color = White),
    h6 = this.h6.copy(fontFamily = fontFamily, color = White),
    subtitle1 = this.subtitle1.copy(fontFamily = fontFamily, color = MediumGrey),
    subtitle2 = this.subtitle2.copy(fontFamily = fontFamily, color = MediumGrey),
    body1 = this.body1.copy(fontFamily = fontFamily, color = White),
    body2 = this.body2.copy(fontFamily = fontFamily, color = White, fontWeight = FontWeight.Bold),
    button = this.button.copy(fontFamily = fontFamily, color = White),
    caption = this.caption.copy(fontFamily = fontFamily, color = MediumGrey),
    overline = this.overline.copy(fontFamily = fontFamily, color = White),
)

internal val LocalTypography = staticCompositionLocalOf { AppTypography }