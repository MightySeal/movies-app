package io.kzolotov.moviesapp.design.component

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import io.kzolotov.moviesapp.design.AppTheme
import io.kzolotov.moviesapp.design.DarkClay
import io.kzolotov.moviesapp.design.White

@Composable
fun TextInput(
    label: String,
    searchQuery: String,
    modifier: Modifier = Modifier,
    onInput: (String) -> Unit
) {

    TextField(
        value = searchQuery,
        placeholder = { AppText(text = label, style = AppTheme.typography.caption) },
        modifier = modifier,
        onValueChange = onInput,
        colors = TextFieldDefaults.outlinedTextFieldColors(
            cursorColor = White,
            textColor = White,
            backgroundColor = DarkClay,
            focusedBorderColor = Color.Transparent,
            unfocusedBorderColor = Color.Transparent,
            disabledBorderColor = Color.Transparent,
            errorBorderColor = Color.Transparent,
        ),
        shape = RoundedCornerShape(150.dp),
        maxLines = 1,
        singleLine = true
    )
}