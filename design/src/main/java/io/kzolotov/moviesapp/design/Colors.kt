package io.kzolotov.moviesapp.design

import androidx.compose.runtime.Immutable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color

val DarkGrey = Color(0xff1f2635)
val CrimsonRed = Color(0xfff4376d)
val Grey = Color(0xff8d99b2)
val LightGrey = Color(0xffdadada)
val MediumGrey = Color(0xff9a9a9a)
val DarkClay = Color(0xff222939)
val Navy = Color(0xff576e98)
val NavyPale = Color(0xff475165)
val NavyDark = Color(0xff283040)
val GreyNeutral = Color(0xff72787d)
val White = Color.White
val Violet = Color(0xff4634a9)
val CrimsonRedDark = Color(0xfff63669)

@Immutable
data class Colors(
    val background: Color
)

val AppColors = Colors(
    background = DarkGrey
)

internal val LocalColors = staticCompositionLocalOf { AppColors }