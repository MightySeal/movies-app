package io.kzolotov.moviesapp.design.component

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import io.kzolotov.moviesapp.design.AppTheme

@Composable
fun AppText(
    text: String,
    modifier: Modifier = Modifier,
    style: TextStyle = AppTheme.typography.body1
) = Text(
    text = text,
    style = style,
    modifier = modifier
)

@Composable
fun AppText(
    text: AnnotatedString,
    modifier: Modifier = Modifier,
    style: TextStyle = AppTheme.typography.body1
) = Text(
    text = text,
    style = style,
    modifier = modifier
)