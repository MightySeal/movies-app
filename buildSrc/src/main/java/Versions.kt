object Versions {
    const val BUILD_TOOLS_VERSION = "7.0.0"
    const val COMPOSE_VERSION = "1.0.2"

    // const val KOTLIN_VERSION = "1.5.30" while compose uses 21
    const val KOTLIN_VERSION = "1.5.21"
    const val HILT_VERSION = "2.38.1"

    const val ACTIVITY_COMPOSE_VERSION = "1.3.1"
    const val CONSTRAINT_COMPOSE_VERSION = "1.0.0-alpha08"

    const val OKIO_VERSION = "2.10.0"
    const val OKHTTP_VERSION = "4.9.1"
    const val RETROFIT_VERSION = "2.9.0"
    const val SERIALIZATION_VERSION = "1.2.2"
    const val SERIALIZATION_RETROFIT_ADAPTER_VERSION = "0.8.0"

    const val COROUTINES = "1.5.0"

    const val SQLDELIGHT = "1.5.0"
}