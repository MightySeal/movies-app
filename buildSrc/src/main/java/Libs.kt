object Libs {

    const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.COROUTINES}"
    const val COROUTINES_ANDROID =
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.COROUTINES}"

    const val OKIO = "com.squareup.okio:okio:${Versions.OKIO_VERSION}"
    const val OKHTTP = "com.squareup.okhttp3:okhttp:${Versions.OKHTTP_VERSION}"
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${Versions.RETROFIT_VERSION}"

    const val HILT = "com.google.dagger:hilt-android:${Versions.HILT_VERSION}"
    const val HILT_COMPILER = "com.google.dagger:hilt-compiler:${Versions.HILT_VERSION}"

    const val SQLDELIGHT_PLUGIN = "com.squareup.sqldelight:gradle-plugin:${Versions.SQLDELIGHT}"
    const val SQLDELIGHT_DRIVER_ANDROID =
        "com.squareup.sqldelight:android-driver:${Versions.SQLDELIGHT}"
    const val SQLDELIGHT_COROUTINES =
        "com.squareup.sqldelight:coroutines-extensions:${Versions.SQLDELIGHT}"

    const val TIMBER = "com.jakewharton.timber:timber:4.7.1"
}