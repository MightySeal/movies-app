package io.kzolotov.moviesapp.data.repository

import io.kzolotov.moviesapp.data.core.AppCoroutineDispatchers
import io.kzolotov.moviesapp.data.network.MoviesService
import io.kzolotov.moviesapp.data.network.model.MovieItem
import io.kzolotov.moviesapp.data.network.model.Response
import io.kzolotov.moviesapp.data.provider.MoviesDB
import io.kzolotov.moviesapp.domain.model.Movie
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.test.assertEquals

@OptIn(ExperimentalCoroutinesApi::class)
class MoviesRepositoryImplTest {

    private val moviesService: MoviesService = mockk()
    private val moviesDb: MoviesDB = mockk()
    private val testDispatcher = TestCoroutineDispatcher()

    private lateinit var underTest: MoviesRepositoryImpl

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        underTest = MoviesRepositoryImpl(moviesService, moviesDb, AppCoroutineDispatchers(testDispatcher, testDispatcher, testDispatcher, testDispatcher))
    }

    @After
    fun tearDown() {
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `getAll emits exactly twice`() = testDispatcher.runBlockingTest {
        val remoteMovie = MovieItem(1, "Title", "2015", "Genre", "a-poster-url")
        val localMovie = Movie(2, "Title 2", 2017, "Genre 2", "no-poster")

        coEvery { moviesService.getMovies() } returns Response(listOf(remoteMovie))
        coEvery { moviesDb.getAll(any(), any()) } returns listOf(localMovie)
        coEvery { moviesDb.update(any()) } just Runs

        val emits = underTest.getAll().toList()

        assertEquals(2, emits.count())
    }

    @Test
    fun `getAll emits only different lists`() = testDispatcher.runBlockingTest {
        val remoteMovie = MovieItem(1, "Title", "2015", "Genre", "a-poster-url")
        val localMovie = Movie(1, "Title", 2015, "Genre", "a-poster-url")

        coEvery { moviesService.getMovies() } returns Response(listOf(remoteMovie))
        coEvery { moviesDb.getAll(any(), any()) } returns listOf(localMovie)
        coEvery { moviesDb.update(any()) } just Runs

        val emits = underTest.getAll().toList()

        assertEquals(1, emits.count())
    }

    @Test
    fun `getAll updates cache`() = testDispatcher.runBlockingTest {
        val remoteMovie = MovieItem(1, "Title", "2015", "Genre", "a-poster-url")
        val localMovie = Movie(1, "Title", 2015, "Genre", "a-poster-url")

        coEvery { moviesService.getMovies() } returns Response(listOf(remoteMovie))
        coEvery { moviesDb.getAll(any(), any()) } returns listOf(localMovie)
        coEvery { moviesDb.update(any()) } just Runs

        val emits = underTest.getAll().toList()

        coVerify { moviesDb.update(listOf(remoteMovie)) }
        assertEquals(emits.size, 1)
    }
}