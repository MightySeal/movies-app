package io.kzolotov.moviesapp.data.network

import io.kzolotov.moviesapp.data.network.model.MovieItem
import io.kzolotov.moviesapp.data.network.model.Response
import retrofit2.http.GET

internal interface MoviesService {

    @GET("api/movies")
    suspend fun getMovies(): Response<List<MovieItem>>

    companion object {
        const val ENDPOINT = "https://movies-sample.herokuapp.com/"
    }
}