package io.kzolotov.moviesapp.data.di

import android.content.Context
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import io.kzolotov.moviesapp.data.Database
import io.kzolotov.moviesapp.sqldelight.data.movie.MovieEntityQueries
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): Database {
        val driver: SqlDriver = AndroidSqliteDriver(Database.Schema, context, "movie.db")
        return Database(driver)
    }

    @Provides
    @Singleton
    fun provideMovieEntityQueries(database: Database): MovieEntityQueries {
        return database.movieEntityQueries
    }
}
