package io.kzolotov.moviesapp.data.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.kzolotov.moviesapp.data.repository.MoviesRepositoryImpl
import io.kzolotov.moviesapp.domain.repository.MoviesRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindMovieRepository(repo: MoviesRepositoryImpl): MoviesRepository
}
