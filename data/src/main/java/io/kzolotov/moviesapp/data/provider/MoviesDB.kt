package io.kzolotov.moviesapp.data.provider

import io.kzolotov.moviesapp.data.network.model.MovieItem
import io.kzolotov.moviesapp.data.repository.model.mapRaw
import io.kzolotov.moviesapp.domain.model.Movie
import io.kzolotov.moviesapp.sqldelight.data.movie.MovieEntityQueries
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class MoviesDB @Inject constructor(
    private val movieEntityQueries: MovieEntityQueries,
    private val clock: ClockProvider
) {

    suspend fun getAll(newerThan: Int, unit: TimeUnit): List<Movie> = movieEntityQueries
        .selectAll(cacheTimestamp(newerThan, unit), ::mapRaw)
        .executeAsList()

    suspend fun search(query: String, newerThan: Int, unit: TimeUnit): List<Movie> =
        movieEntityQueries
            .search(query, cacheTimestamp(newerThan, unit), ::mapRaw)
            .executeAsList()

    suspend fun update(movies: List<MovieItem>) {
        val currentTimestamp = clock.currentTimestamp
        movieEntityQueries.transaction {
            movies.forEach { movie ->
                movieEntityQueries.update(
                    movie.id,
                    movie.title,
                    movie.year.toInt(),
                    movie.genre,
                    movie.posterUrl,
                    currentTimestamp
                )
            }
        }
    }

    private fun cacheTimestamp(newerThan: Int, unit: TimeUnit): Long =
        clock.currentTimestamp - unit.toMillis(newerThan.toLong())
}