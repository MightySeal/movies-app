package io.kzolotov.moviesapp.data.repository.model

import io.kzolotov.moviesapp.data.network.model.MovieItem
import io.kzolotov.moviesapp.domain.model.Movie

internal fun MovieItem.toDomain(): Movie = Movie(
    id = id,
    title = title,
    year = year.toInt(),
    genre = genre,
    poster = posterUrl
)

internal fun mapRaw(
    id: Long,
    title: String,
    year: Int,
    genre: String,
    poster: String,
    timestamp: Long
): Movie = Movie(
    id = id,
    title = title,
    year = year,
    genre = genre,
    poster = poster,
    timestamp = timestamp
)