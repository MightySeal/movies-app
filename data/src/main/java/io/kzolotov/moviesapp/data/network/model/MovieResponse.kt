package io.kzolotov.moviesapp.data.network.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class MovieItem(
    @SerialName("id")
    val id: Long,
    @SerialName("title")
    val title: String,
    @SerialName("year")
    val year: String,
    @SerialName("genre")
    val genre: String,
    @SerialName("poster")
    val posterUrl: String
)

@Serializable
internal data class Response<T>(
    @SerialName("data")
    val data: T
)