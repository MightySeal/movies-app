package io.kzolotov.moviesapp.data.provider

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
internal class ClockProvider @Inject constructor() {
    val currentTimestamp: Long
        get() = System.currentTimeMillis()
}