package io.kzolotov.moviesapp.data.repository

import io.kzolotov.moviesapp.data.core.AppCoroutineDispatchers
import io.kzolotov.moviesapp.data.network.MoviesService
import io.kzolotov.moviesapp.data.network.model.MovieItem
import io.kzolotov.moviesapp.data.provider.MoviesDB
import io.kzolotov.moviesapp.data.repository.model.toDomain
import io.kzolotov.moviesapp.domain.model.Movie
import io.kzolotov.moviesapp.domain.repository.MoviesRepository
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flow
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoviesRepositoryImpl @Inject internal constructor(
    private val moviesService: MoviesService,
    private val moviesDB: MoviesDB,
    private val dispatchers: AppCoroutineDispatchers
) : MoviesRepository {

    override fun getAll(): Flow<List<Movie>> {
        return flow {
            coroutineScope {
                val local = async(dispatchers.io) {
                    moviesDB.getAll(CACHE_TIME_MINUTES, TimeUnit.MINUTES)
                }

                val remote = async(dispatchers.io) {
                    moviesService.getMovies().data
                        .also { moviesDB.update(it) }
                        .map(MovieItem::toDomain)
                        .sortedBy(Movie::id)
                }

                emit(local.await())
                emit(remote.await())
            }
        }.distinctUntilChanged()
    }

    override fun query(query: String): Flow<List<Movie>> {
        return flow {
            coroutineScope {
                val local = async(dispatchers.io) {
                    moviesDB.search(query, CACHE_TIME_MINUTES, TimeUnit.MINUTES)
                }

                val remote = async(dispatchers.io) {
                    moviesService.getMovies().data.also { moviesDB.update(it) }
                        .map(MovieItem::toDomain)
                        .filter { movie ->
                            movie.title.contains(query, true) || movie.genre.contains(query, true)
                        }
                }

                emit(local.await())
                emit(remote.await())
            }
        }.distinctUntilChanged()
    }

    companion object {
        private const val CACHE_TIME_MINUTES = 10
    }
}