package io.kzolotov.moviesapp.data.core

import kotlinx.coroutines.CoroutineDispatcher

class AppCoroutineDispatchers(
    val default: CoroutineDispatcher,
    val io: CoroutineDispatcher,
    val main: CoroutineDispatcher,
    val unconfined: CoroutineDispatcher
)