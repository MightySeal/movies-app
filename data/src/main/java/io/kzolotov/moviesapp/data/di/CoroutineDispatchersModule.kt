package io.kzolotov.moviesapp.data.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.kzolotov.moviesapp.data.core.AppCoroutineDispatchers
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object CoroutineDispatchersModule {

    @Provides
    @Singleton
    fun provideDatabase(): AppCoroutineDispatchers {
        return AppCoroutineDispatchers(
            default = Dispatchers.Default,
            io = Dispatchers.IO,
            main = Dispatchers.Main,
            unconfined = Dispatchers.Unconfined
        )
    }
}