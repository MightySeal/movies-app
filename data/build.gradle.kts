plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
    id("dagger.hilt.android.plugin")
    kotlin("plugin.serialization") version Versions.KOTLIN_VERSION
    id("com.squareup.sqldelight")
}

android {
    compileSdk = 30
    buildToolsVersion = "31.0.0"

    defaultConfig {
        minSdk = 21
        targetSdk = 31

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    kotlinOptions {
        jvmTarget = "11"
    }
}

dependencies {
    implementation(project(":domain"))

    implementation(Libs.COROUTINES_ANDROID)
    implementation(Libs.TIMBER)

    implementation(Libs.OKIO)
    implementation(Libs.OKHTTP)
    implementation(Libs.RETROFIT)

    implementation(Libs.HILT)
    kapt(Libs.HILT_COMPILER)

    implementation(Libs.SQLDELIGHT_DRIVER_ANDROID)
    implementation(Libs.SQLDELIGHT_COROUTINES)

    implementation("com.squareup.okhttp3:logging-interceptor:${Versions.OKHTTP_VERSION}")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.SERIALIZATION_VERSION}")
    implementation("com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:${Versions.SERIALIZATION_RETROFIT_ADAPTER_VERSION}")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.COROUTINES}")
    testImplementation("io.mockk:mockk:1.9.3")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:${Versions.KOTLIN_VERSION}")
}

hilt {
    enableAggregatingTask = true
}